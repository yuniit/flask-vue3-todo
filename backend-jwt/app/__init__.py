from flask import Flask
from db import Connection
from config import Config
from flask_jwt_extended import JWTManager
from flask_cors import CORS
import datetime

db = Connection("flask_mongo_db_jwt")

def create_app():
    app = Flask(__name__)
    app.secret_key = 'rabbit_tech'
    
    JWTManager(app) # initialize JWTManager
    app.config['JWT_SECRET_KEY'] = Config.secret_key
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1) # define the life span of the token

    CORS(app)
    app.config.from_object(Config)

    ## register blueprint
    from app.auth import bp as auth_bp
    from app.todo import bp as todo_bp

    app.register_blueprint(auth_bp, url_prefix="/auth")
    app.register_blueprint(todo_bp, url_prefix="/todo")

    @app.route("/", methods=("GET", "POST"))
    def index():
        return "hello world"

    return app
