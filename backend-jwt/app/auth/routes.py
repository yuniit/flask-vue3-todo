from flask import request, Response, jsonify
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
import datetime
from app.auth import bp
from app.models.users import User


@bp.get("/getCurrentUser")
@jwt_required()
def profile():
    userId = get_jwt_identity()
    current_user = User().get_by_id(userId=userId)
    if current_user is not None:
        return jsonify({"current_user": current_user}), 200
    else:
        return Response(
            "Current login user is not found",
            status=404,
        )


@bp.post("/login")
def login():
    data = request.get_json()
    user = User().get_by_username(username=data["username"])

    if user is not None and User.check_password(
        hashed_password=user["password"], password=data["password"]
    ):
        access_token = create_access_token(
            identity=user["_id"], expires_delta=datetime.timedelta(days=1)
        )  # create jwt token for userId

        return jsonify(access_token=access_token), 200

    return Response(
        "Username or password is incorrect",
        status=400,
    )


# @bp.route("/logout", methods=["GET", "POST"])
# @jwt_required()
# def logout():

#     return "success"


@bp.post("/register")
def register():
    data = request.get_json()

    user = User(username=data["username"], password=data["password"])

    # Hashing password
    user.set_password(password=data["password"])

    result = user.register()

    if not result.inserted_id:
        return {"message": "Failed to register"}, 500

    return {"message": "Success", "data": {"id": result.inserted_id}}, 200
