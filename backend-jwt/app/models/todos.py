from bson.objectid import ObjectId
from app import db


class Todo:
    def __init__(self, title=None, userId=None, _id=None):
        self.title = title
        self.userId = userId
        self._id = str(ObjectId()) if _id is None else _id

    def get_by_user(self, userId):
        result = list(db.todos.find({"userId": userId}))
        return result

    def get__data_table_by_user(self, userId, page, limit):
        skip = (page - 1) * limit
        data = list(db.todos.find({"userId": userId}).skip(skip).limit(limit))
        total = db.todos.count_documents({"userId": userId})

        result = dict()
        result["data"] = data
        result["total"] = total
        return result

    def insert(self, insertCount=1):
        result = db.todos.insert_many(
            [self.to_dict(index=i) for i in range(insertCount)]
        )
        return result

    def delete(self, _id):
        result = db.todos.delete_one({"_id": _id})
        return result

    def deleteAll(self, userId: str):
        result = db.todos.delete_many({"userId": userId})
        return result

    def to_dict(self, index: int):
        """Transform to dict to insert into mongo collection."""
        _id = self._id if index is None else self._id + "-" + str(index)
        return {"_id": _id, "title": self.title, "userId": self.userId}
