from bson.objectid import ObjectId
import hashlib
from app import db


class User:
    def __init__(self, username=None, password=None, _id=None):
        self.username = username
        self.password = password
        self._id = str(ObjectId()) if _id is None else _id

    @classmethod
    def set_password(cls, password):
        cls.password_hash = cls.encrypt_password(password)

    @staticmethod
    def encrypt_password(password: str):
        return hashlib.sha256(password.encode("utf-8")).hexdigest()

    @staticmethod
    def check_password(hashed_password: str, password: str):
        return hashlib.sha256(password.encode("utf-8")).hexdigest() == hashed_password

    def get_by_username(self, username: str):
        return db.users.find_one({"username": username})

    def get_by_id(self, userId: str):
        return db.users.find_one({"_id": userId})

    def register(self):
        result = db.users.insert_one(self.to_dict())
        print(f"{self.to_dict()} entry created.")
        return result

    def to_dict(self):
        """Transform to dict to insert into mongo collection."""
        return {
            "_id": self._id,
            "username": self.username,
            "password": self.password_hash,
        }
