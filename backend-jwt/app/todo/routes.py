from app.todo import bp
from flask import request, jsonify
from flask_jwt_extended import jwt_required
from app.models.todos import Todo



@bp.route("/getDataTable", methods=["GET"])
@jwt_required()
def getDataTable():
    if request.method == "GET":
        userId = request.args.get("userId")
        page =int(request.args.get("page"))
        pageSize = int (request.args.get("pageSize"))
   
        result = Todo().get__data_table_by_user(userId=userId,page=page,limit=pageSize)
        return jsonify(result), 200


@bp.route("/insert", methods=["POST"])
@jwt_required()
def insert():
    if request.method == "POST":
        data = request.get_json()
        
        insertCount  = int(data["insertCount"]) if data["insertCount"] else 1
        
        todo = Todo(title=data["title"], userId=data["userId"])
        todo.insert(insertCount=insertCount)

        return "Success", 200


@bp.delete("/delete")
@jwt_required()
def delete():
    _id = request.args.get("_id")
    Todo().delete(_id=_id)
    return "Success", 200

@bp.delete("/deleteAll")
@jwt_required()
def deleteAll():
    userId = request.args.get("userId")
    Todo().deleteAll(userId=userId)
    return "Success", 200
